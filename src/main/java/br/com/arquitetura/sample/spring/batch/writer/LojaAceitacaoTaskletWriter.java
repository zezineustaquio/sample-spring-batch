package br.com.arquitetura.sample.spring.batch.writer;

import java.util.List;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.arquitetura.sample.spring.db.oracle.trb013.entity.LojaAceitacaoEntity;
import br.com.arquitetura.sample.spring.db.oracle.trb013.repository.LojaAceitacaoRepository;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Component
public class LojaAceitacaoTaskletWriter implements Tasklet, StepExecutionListener {

	@Autowired
	private LojaAceitacaoRepository repository;

	private List<LojaAceitacaoEntity> lstLojasAceitacao;

	@SuppressWarnings("unchecked")
	@Override
	public void beforeStep(StepExecution stepExecution) {
		log.info("LojaAceitacaoWriter iniciado...");
		lstLojasAceitacao = (List<LojaAceitacaoEntity>) stepExecution.getJobExecution().getExecutionContext()
				.get("lstLojasAceitacao");
	}

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		log.info("Persistir lojas da Rede de aceitação ...");
		repository.saveAll(lstLojasAceitacao);
		log.info("Registros inseridos/atualizados com sucesso!");
		return RepeatStatus.FINISHED;
	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		stepExecution.getJobExecution().getExecutionContext().remove("lstLojasAceitacao");
		log.info("LojaAceitacaoWriter executado!");
		return ExitStatus.COMPLETED;
	}

}
