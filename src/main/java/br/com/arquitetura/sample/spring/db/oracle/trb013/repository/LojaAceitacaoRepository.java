package br.com.arquitetura.sample.spring.db.oracle.trb013.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.arquitetura.sample.spring.db.oracle.trb013.entity.LojaAceitacaoEntity;

@Repository
public interface LojaAceitacaoRepository extends CrudRepository<LojaAceitacaoEntity, String> {

	@Query(value = "" //
			+ " SELECT " //
			+ "   loja" //
			+ " FROM" //
			+ "   LojaAceitacaoEntity loja" //
			+ "   INNER JOIN FETCH loja.mapaClientes mapa" //
			+ " WHERE" //
			+ "   loja.dsLatitude IS NULL " //
			+ "   OR mapa.dataUltimaAtualizacao IS NULL " //
			+ "   OR mapa.dataUltimaAtualizacao < :dataAtualizacaoMinima")
	public List<LojaAceitacaoEntity> findLojaAceitacaoToLocate(Date dataAtualizacaoMinima);

	@Query(nativeQuery = true, value = "SELECT 1 FROM DUAL")
	public int healthCheck();
}
