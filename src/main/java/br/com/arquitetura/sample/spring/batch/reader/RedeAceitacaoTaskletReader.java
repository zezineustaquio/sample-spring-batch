package br.com.arquitetura.sample.spring.batch.reader;

import java.util.List;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.arquitetura.sample.spring.db.sql.crp.entity.RedeAceitacaoEntity;
import br.com.arquitetura.sample.spring.db.sql.crp.repository.RedeAceitacaoRepository;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Component
public class RedeAceitacaoTaskletReader implements Tasklet, StepExecutionListener {

	@Autowired
	private RedeAceitacaoRepository repository;

	private List<RedeAceitacaoEntity> lstRedeAceitacao;

	@Override
	public void beforeStep(StepExecution stepExecution) {
		log.info("RedeAceitacaoReader iniciado...");
	}

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		log.info("Consulta das lojas de aceitacao ...");
		lstRedeAceitacao = repository.listRedeAceitacao();
		log.info("Consulta retornou {} registros de lojas!", lstRedeAceitacao.size());
		return RepeatStatus.FINISHED;
	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		stepExecution.getJobExecution().getExecutionContext().put("lstRedeAceitacao", lstRedeAceitacao);
		log.info("RedeAceitacaoReader executado!");
		return ExitStatus.COMPLETED;
	}

}
