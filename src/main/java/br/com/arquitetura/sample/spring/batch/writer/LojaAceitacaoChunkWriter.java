package br.com.arquitetura.sample.spring.batch.writer;

import java.util.List;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.arquitetura.sample.spring.db.oracle.trb013.entity.LojaAceitacaoEntity;
import br.com.arquitetura.sample.spring.db.oracle.trb013.repository.LojaAceitacaoRepository;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Component
public class LojaAceitacaoChunkWriter implements ItemWriter<LojaAceitacaoEntity>, StepExecutionListener {

	@Autowired
	private LojaAceitacaoRepository repository;

	@Override
	public void beforeStep(StepExecution stepExecution) {
		log.info("LojaAceitacaoChunkWriter iniciado...");
	}

	@Override
	public void write(List<? extends LojaAceitacaoEntity> items) throws Exception {
		log.info("Persistir lojas da Rede de aceitação ...");
		repository.saveAll(items);
		log.info("Registros inseridos/atualizados com sucesso!");
	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		log.info("LojaAceitacaoChunkWriter executado!");
		return ExitStatus.COMPLETED;
	}

}
