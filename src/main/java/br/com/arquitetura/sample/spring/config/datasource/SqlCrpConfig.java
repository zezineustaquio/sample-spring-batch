package br.com.arquitetura.sample.spring.config.datasource;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

@Profile("!test")
@Configuration
@EnableTransactionManagement
@ConfigurationProperties(prefix = "datasource.sql.crp")
@EnableJpaRepositories(//
		basePackages = "br.com.arquitetura.sample.spring.db.sql.crp.repository", //
		entityManagerFactoryRef = "sql-crp-em", //
		transactionManagerRef = "sql-crp-tm")
public class SqlCrpConfig extends HikariConfig {
	/**
	 * Factory para criação do Data Source
	 * 
	 * @return
	 */
	@Bean(name = "sql-crp-ds")
	public DataSource sqlCrpDataSourceFactory() {
		return new HikariDataSource(this);
	}

	/**
	 * Factory para criação do Entity Manager
	 * 
	 * @param builder
	 * @return
	 */
	@PersistenceContext(unitName = "CRP")
	@Bean(name = "sql-crp-em")
	public LocalContainerEntityManagerFactoryBean sqlCrpEntityManagerFactory(EntityManagerFactoryBuilder builder) {
		return builder.dataSource(sqlCrpDataSourceFactory()).persistenceUnit("CRP").properties(jpaProperties())
				.packages("br.com.arquitetura.sample.spring.db.sql.crp.entity").build();
	}

	/**
	 * Factory para criação do Transaction Manager
	 * 
	 * @param em
	 * @return
	 */
	@Bean(name = "sql-crp-tm")
	public PlatformTransactionManager sqlCrpTransactionManagerFactory(
			@Qualifier("sql-crp-em") EntityManagerFactory em) {
		return new JpaTransactionManager(em);
	}

	/**
	 * Propriedades do Persistence Unity
	 * 
	 * @return
	 */
	private Map<String, Object> jpaProperties() {
		Map<String, Object> props = new HashMap<>();
		props.put("hibernate.dialect", "org.hibernate.dialect.SQLServer2005Dialect");
		return props;
	}
}
