package br.com.arquitetura.sample.spring.db.sql.crp07.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NamedNativeQueries({ //
		@NamedNativeQuery(name = "TipoEstabelecimentoCnaeEntity.listTipoEstabelecimentoCnae", //
				query = " SELECT DISTINCT" + //
						"  tpEst.CD_TIPO_ESTABELECIMENTO, " + //
						"  cnae.CD_CNAE " + //
						" FROM" + //
						"  db_sgad.dbo.TBL_MCC_X_TIPO_ESTABELECIMENTO tpEst WITH(NOLOCK)" + //
						"  INNER JOIN db_sgad.dbo.TBL_MCC_X_CNAE mccCnae WITH(NOLOCK) ON tpEst.CD_MCC = mccCnae.CD_MCC"
						+ //
						"  INNER JOIN db_sgad.dbo.TBL_CNAE cnae WITH(NOLOCK) ON mccCnae.CD_CNAE = cnae.CD_CNAE" + //
						" WHERE " + //
						"  mccCnae.FL_ATIVO = 1" + //
						"  AND cnae.FL_ATIVO = 1" + //
						"  AND tpEst.FL_ATIVO = 1", //
				resultClass = TipoEstabelecimentoCnaeEntity.class) })
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class TipoEstabelecimentoCnaeEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "CD_CNAE")
	private Long cdCnae;

	@Column(name = "CD_TIPO_ESTABELECIMENTO")
	private Long cdTipoEstabelecimento;
}
