package br.com.arquitetura.sample.spring.db.sql.crp07.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.arquitetura.sample.spring.db.sql.crp07.entity.TipoEstabelecimentoCnaeEntity;

@Repository
public interface TipoEstabelecimentoCnaeRepository extends CrudRepository<TipoEstabelecimentoCnaeEntity, Long> {

	@Query(nativeQuery = true)
	public List<TipoEstabelecimentoCnaeEntity> listTipoEstabelecimentoCnae();
	
	@Query(nativeQuery = true, value = "SELECT 1")
	public int healthCheck();
}
