package br.com.arquitetura.sample.spring.db.sql.crp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.arquitetura.sample.spring.db.sql.crp.entity.RedeAceitacaoEntity;

@Repository
public interface RedeAceitacaoRepository extends CrudRepository<RedeAceitacaoEntity, String> {

	@Query(nativeQuery = true)
	public List<RedeAceitacaoEntity> listRedeAceitacao();
	
	@Query(nativeQuery = true, value = "SELECT 1")
	public int healthCheck();
}
