package br.com.arquitetura.sample.spring.db.oracle.trb013.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.util.StringUtils;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "TBL_LOJA_ACEITACAO", schema = "MEFV")
public class LojaAceitacaoEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "CD_CPF_CNPJ")
	private String cdCpfCnpj;

	@Column(name = "DS_NOME_FANTASIA")
	private String dsNomeFantasia;

	@Column(name = "DS_LOGRADOURO")
	private String dsLogradouro;

	@Column(name = "DS_BAIRRO")
	private String dsBairro;

	@Column(name = "DS_CIDADE")
	private String dsCidade;

	@Column(name = "DS_ESTADO")
	private String dsEstado;

	@Column(name = "DS_CEP")
	private String dsCep;

	@Column(name = "DS_TELEFONE")
	private String dsTelefone;

	@Column(name = "CD_TIPO_BANDEIRA")
	private Long cdTipoBandeira;

	@Column(name = "CD_TIPO_ESTABELECIMENTO")
	private Long cdTipoEstabelecimento;

	@Column(name = "FL_ACEITACAO")
	private Boolean flAceitacao;

	@Column(name = "DS_LATITUDE")
	private BigDecimal dsLatitude;

	@Column(name = "DS_LONGITUDE")
	private BigDecimal dsLongitude;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "CD_CPF_CNPJ", referencedColumnName = "CD_CPF_CNPJ")
	private MapaClientesEntity mapaClientes;

	public String hashCodeAddress() {
		final int prime = 31;
		Integer result = 1;
		result = prime * result + ((cdCpfCnpj == null) ? 0 : cdCpfCnpj.hashCode());
		result = prime * result + ((dsBairro == null) ? 0 : dsBairro.hashCode());
		result = prime * result + ((dsCep == null) ? 0 : dsCep.hashCode());
		result = prime * result + ((dsCidade == null) ? 0 : dsCidade.hashCode());
		result = prime * result + ((dsEstado == null) ? 0 : dsEstado.hashCode());
		result = prime * result + ((dsLogradouro == null) ? 0 : dsLogradouro.hashCode());
		return result.toString();
	}

	public String getEnderecoCompleto() {
		return String.format("%s, %s, %s, %s, %s", dsLogradouro, dsBairro, dsCep, dsCidade, dsEstado);
	}

	public boolean isGeoLocated() {
		if (StringUtils.isEmpty(dsLongitude) && StringUtils.isEmpty(dsLatitude)) {
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}

}
