package br.com.arquitetura.sample.spring.external.dto.geolocation;

public enum StatusEnum {
	OK("OK"), REQUEST_DENIED("REQUEST_DENIED"), OVER_QUERY_LIMIT("OVER_QUERY_LIMIT"), ZERO_RESULTS("ZERO_RESULTS");

	private String status;

	private StatusEnum(String status) {
		this.status = status;
	}

	public String getStatus() {
		return this.status;
	}
}
