package br.com.arquitetura.sample.spring.external.repository;

import br.com.arquitetura.sample.spring.external.dto.geolocation.GeolocationResponseDTO;

@FunctionalInterface
public interface GoogleGeolocationRepository {

	public GeolocationResponseDTO geolocate(String address, String apiToken);
}
