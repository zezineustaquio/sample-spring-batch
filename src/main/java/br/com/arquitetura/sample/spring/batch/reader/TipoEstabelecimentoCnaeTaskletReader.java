package br.com.arquitetura.sample.spring.batch.reader;

import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.arquitetura.sample.spring.db.sql.crp07.entity.TipoEstabelecimentoCnaeEntity;
import br.com.arquitetura.sample.spring.db.sql.crp07.repository.TipoEstabelecimentoCnaeRepository;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Component
public class TipoEstabelecimentoCnaeTaskletReader implements Tasklet, StepExecutionListener {

	@Autowired
	private TipoEstabelecimentoCnaeRepository tipoEstabelecimentoCnaeRepository;

	private Map<Long, Long> mapTipoEstabelecimentoCnae;

	@Override
	public void beforeStep(StepExecution stepExecution) {
		log.info("TipoEstabelecimentoCnaeReader iniciado...");

	}

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		log.info("Consultando tipos de estabelecimentos ...");
		mapTipoEstabelecimentoCnae = tipoEstabelecimentoCnaeRepository.listTipoEstabelecimentoCnae().stream()
				.collect(Collectors.toMap(TipoEstabelecimentoCnaeEntity::getCdCnae,
						TipoEstabelecimentoCnaeEntity::getCdTipoEstabelecimento));
		log.info("Consulta retornou {} tipos de estabelecimentos!", mapTipoEstabelecimentoCnae.size());
		return RepeatStatus.FINISHED;
	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		stepExecution.getJobExecution().getExecutionContext().put("mapTipoEstabelecimentoCnae",
				mapTipoEstabelecimentoCnae);
		log.info("TipoEstabelecimentoCnaeReader executado!");
		return ExitStatus.COMPLETED;
	}

}
