package br.com.arquitetura.sample.spring.external.dto.geolocation;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class GeometryDTO {

	@JsonProperty(value = "location_type")
	private String locationType;

	private LocationDTO location;
}
