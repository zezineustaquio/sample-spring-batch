package br.com.arquitetura.sample.spring.external.dto.geolocation;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class GeolocationResponseDTO {

	@JsonProperty(value = "error_message")
	private String errorMessage;
	private String status;
	private List<ResultDTO> results;
}
