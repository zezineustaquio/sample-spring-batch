package br.com.arquitetura.sample.spring.batch.job;

import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.job.builder.FlowBuilder;
import org.springframework.batch.core.job.flow.Flow;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.stereotype.Component;

import br.com.arquitetura.sample.spring.batch.processor.RedeAceitacaoTaskletProcessor;
import br.com.arquitetura.sample.spring.batch.reader.LojaAceitacaoTaskletReader;
import br.com.arquitetura.sample.spring.batch.reader.RedeAceitacaoTaskletReader;
import br.com.arquitetura.sample.spring.batch.reader.TipoEstabelecimentoCnaeTaskletReader;
import br.com.arquitetura.sample.spring.batch.writer.LojaAceitacaoTaskletWriter;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Component
public class RedeAceitacaoJob extends JobExecutionListenerSupport {

	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	@Autowired
	private StepBuilderFactory stepBuilderFactory;

	@Autowired
	private RedeAceitacaoTaskletReader redeAceitacaoReader;

	@Autowired
	private LojaAceitacaoTaskletReader lojaAceitacaoReader;

	@Autowired
	private TipoEstabelecimentoCnaeTaskletReader tipoEstabelecimentoCnaeReader;

	@Autowired
	private RedeAceitacaoTaskletProcessor processor;

	@Autowired
	private LojaAceitacaoTaskletWriter writer;

	@Bean(name = "RedeAceitacaoJob")
	public Job redeAceitacaoJob() {

		Flow redeAceitacao = new FlowBuilder<Flow>("rede-aceitacao") //
				.from(stepBuilderFactory //
						.get("rede-aceitacao-reader") //
						.tasklet(redeAceitacaoReader) //
						.allowStartIfComplete(Boolean.TRUE) //
						.build())
				.end();

		Flow lojaAceitacao = new FlowBuilder<Flow>("loja-aceitacao") //
				.from(stepBuilderFactory //
						.get("loja-aceitacao-reader") //
						.tasklet(lojaAceitacaoReader) //
						.allowStartIfComplete(Boolean.TRUE) //
						.build())
				.end();

		Flow tipoEstabelecimentoCnae = new FlowBuilder<Flow>("tipo-estabelecimento-cnae") //
				.from(stepBuilderFactory //
						.get("tipo-estabelecimento-cnae-reader") //
						.tasklet(tipoEstabelecimentoCnaeReader) //
						.allowStartIfComplete(Boolean.TRUE) //
						.build())
				.end();

		Flow parallel = new FlowBuilder<Flow>("parallel-flow") //
				.start(redeAceitacao) //
				.split(new SimpleAsyncTaskExecutor()) //
				.add(lojaAceitacao, tipoEstabelecimentoCnae) //
				.build();

		return jobBuilderFactory.get("rede-aceitacao-job") //
				.incrementer(new RunIdIncrementer()) //
				.start(parallel) //
				.next(stepBuilderFactory //
						.get("rede-aceitacao-processor") //
						.tasklet(processor) //
						.allowStartIfComplete(Boolean.TRUE) //
						.build()) //
				.next(stepBuilderFactory //
						.get("rede-aceitacao-writer") //
						.tasklet(writer) //
						.allowStartIfComplete(Boolean.TRUE) //
						.build()) //
				.build() // Build Flow Job
				.build(); // Build Job
	}

	@Override
	public void afterJob(JobExecution jobExecution) {
		if (jobExecution.getStatus() == BatchStatus.COMPLETED) {
			log.info("Job redeAceitacaoJob executada com sucesso!");
		} else {
			log.error("Job redeAceitacaoJob terminada com status {}!", jobExecution.getStatus().toString());
		}
	}

}
