package br.com.arquitetura.sample.spring.batch.job;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import br.com.arquitetura.sample.spring.batch.processor.GeolocalizacaoChunkProcessor;
import br.com.arquitetura.sample.spring.batch.reader.LojaAceitacaoToLocateChunkReader;
import br.com.arquitetura.sample.spring.batch.writer.LojaAceitacaoChunkWriter;
import br.com.arquitetura.sample.spring.db.oracle.trb013.entity.LojaAceitacaoEntity;

@Component
public class GeolocalizacaoJob extends JobExecutionListenerSupport {

	@Autowired
	private JobBuilderFactory jobBuilderFactory;

	@Autowired
	private StepBuilderFactory stepBuilderFactory;

	@Autowired
	private LojaAceitacaoToLocateChunkReader reader;

	@Autowired
	private GeolocalizacaoChunkProcessor processor;

	@Autowired
	private LojaAceitacaoChunkWriter writer;

	@Bean(name = "GeolocalizacaoJob")
	public Job geolocalizacaoJob() {
		return jobBuilderFactory.get("geolocalizacao-job") //
				.start(stepBuilderFactory //
						.get("geolocate-items").<LojaAceitacaoEntity, LojaAceitacaoEntity>chunk(1) //
						.reader(reader) //
						.writer(writer) //
						.processor(processor) //
						.allowStartIfComplete(Boolean.TRUE) //
						.build()) //
				.build();
	}
}
