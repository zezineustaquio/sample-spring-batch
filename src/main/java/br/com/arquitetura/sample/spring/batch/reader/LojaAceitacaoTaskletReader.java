package br.com.arquitetura.sample.spring.batch.reader;

import java.util.List;

import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.arquitetura.sample.spring.db.oracle.trb013.entity.LojaAceitacaoEntity;
import br.com.arquitetura.sample.spring.db.oracle.trb013.repository.LojaAceitacaoRepository;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Component
public class LojaAceitacaoTaskletReader implements Tasklet, StepExecutionListener {

	@Autowired
	private LojaAceitacaoRepository lojaAceitacaoRepository;

	private List<LojaAceitacaoEntity> lstLojasAtuais;

	@Override
	public void beforeStep(StepExecution stepExecution) {
		log.info("LojaAceitacaoReader iniciado...");

	}

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		log.info("Buscando lojas para atualizacao se necessário ...");
		lstLojasAtuais = (List<LojaAceitacaoEntity>) lojaAceitacaoRepository.findAll();
		log.info("Consulta das lojas concluidas!");
		return RepeatStatus.FINISHED;
	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		stepExecution.getJobExecution().getExecutionContext().put("lstLojasAtuais", lstLojasAtuais);
		log.info("LojaAceitacaoReader executado!");
		return ExitStatus.COMPLETED;
	}

}
