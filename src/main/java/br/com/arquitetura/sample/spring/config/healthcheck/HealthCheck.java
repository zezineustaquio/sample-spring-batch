package br.com.arquitetura.sample.spring.config.healthcheck;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import br.com.arquitetura.healthcheck.DTO.DatabaseDTO;
import br.com.arquitetura.healthcheck.DTO.HealthCheckDTO;
import br.com.arquitetura.healthcheck.service.HealthCheckService;
import br.com.arquitetura.sample.spring.db.oracle.trb013.repository.LojaAceitacaoRepository;
import br.com.arquitetura.sample.spring.db.sql.crp.repository.RedeAceitacaoRepository;
import br.com.arquitetura.sample.spring.db.sql.crp07.repository.TipoEstabelecimentoCnaeRepository;

@Service
public class HealthCheck implements HealthCheckService {

	@Autowired
	private LojaAceitacaoRepository oracleTrb013Repository;

	@Autowired
	private RedeAceitacaoRepository sqlCrpRepository;

	@Autowired
	private TipoEstabelecimentoCnaeRepository sqlCrp07Repository;

	@Value("${datasource.oracle.trb013.jdbcUrl}")
	private String oracleTrb013Url;

	@Value("${datasource.sql.crp.jdbcUrl}")
	private String sqlCrpUrl;

	@Value("${datasource.sql.crp07.jdbcUrl}")
	private String sqlCrp07Url;

	@Value("${google.api.check}")
	private String urlGoogleApis;

	@Override
	public HealthCheckDTO healthCheck() {
		return new HealthCheckDTO() //
				.withUrls(urlGoogleApis) //
				.withDatabases(new DatabaseDTO(oracleTrb013Repository, oracleTrb013Url), //
						new DatabaseDTO(sqlCrpRepository, sqlCrpUrl), //
						new DatabaseDTO(sqlCrp07Repository, sqlCrp07Url));
	}

}
