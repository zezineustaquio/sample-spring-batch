package br.com.arquitetura.sample.spring.batch.processor;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.stereotype.Component;

import br.com.arquitetura.sample.spring.db.oracle.trb013.entity.LojaAceitacaoEntity;
import br.com.arquitetura.sample.spring.db.sql.crp.entity.RedeAceitacaoEntity;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Component
public class RedeAceitacaoTaskletProcessor implements Tasklet, StepExecutionListener {

	private static Long TIPO_BANDEIRA_MASTER = 1L;

	private Map<Long, Long> mapTipoEstabelecimentoCnae;
	private List<RedeAceitacaoEntity> lstRedeAceitacao;
	private List<LojaAceitacaoEntity> lstLojasAceitacao;
	private List<LojaAceitacaoEntity> lstLojasAtuais;

	@SuppressWarnings("unchecked")
	@Override
	public void beforeStep(StepExecution stepExecution) {
		log.info("RedeAceitacaoProcessor iniciado...");
		lstRedeAceitacao = (List<RedeAceitacaoEntity>) stepExecution.getJobExecution().getExecutionContext()
				.get("lstRedeAceitacao");
		lstLojasAtuais = (List<LojaAceitacaoEntity>) stepExecution.getJobExecution().getExecutionContext()
				.get("lstLojasAtuais");
		mapTipoEstabelecimentoCnae = (Map<Long, Long>) stepExecution.getJobExecution().getExecutionContext()
				.get("mapTipoEstabelecimentoCnae");
	}

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {

		if (lstRedeAceitacao != null) {
			lstLojasAceitacao = lstRedeAceitacao.stream()
					.map(rede -> lojaAceitacaoProcessor(mapTipoEstabelecimentoCnae, lstLojasAtuais, rede))
					.collect(Collectors.toList());
			log.info("Processamento da rede concluido!");
		}
		return RepeatStatus.FINISHED;
	}

	@Override
	public ExitStatus afterStep(StepExecution stepExecution) {
		// Removendo objetos que nao serao utilizados
		stepExecution.getJobExecution().getExecutionContext().remove("lstRedeAceitacao");
		stepExecution.getJobExecution().getExecutionContext().remove("lstLojasAtuais");
		stepExecution.getJobExecution().getExecutionContext().remove("mapTipoEstabelecimentoCnae");

		stepExecution.getJobExecution().getExecutionContext().put("lstLojasAceitacao", lstLojasAceitacao);
		log.info("RedeAceitacaoProcessor executado!");
		return ExitStatus.COMPLETED;
	}

	private LojaAceitacaoEntity lojaAceitacaoProcessor(Map<Long, Long> mapTipoEstabelecimentoCnae,
			List<LojaAceitacaoEntity> lojasAtuais, RedeAceitacaoEntity rede) {
		Optional<LojaAceitacaoEntity> lojaUpdate = lojasAtuais.stream()
				.filter(lojaAtual -> lojaAtual.getCdCpfCnpj().equals(rede.getCdCnpj())).findFirst();
		if (lojaUpdate.isPresent()) {
			lojaUpdate.get().setDsNomeFantasia(StringUtils.left(rede.getDsNomeFantasia(), 60));
			lojaUpdate.get().setDsLogradouro(StringUtils.left(rede.getDsLogradouro(), 70));
			lojaUpdate.get().setDsBairro(StringUtils.left(rede.getDsBairro(), 60));
			lojaUpdate.get().setDsCidade(StringUtils.left(rede.getDsCidade(), 60));
			lojaUpdate.get().setDsEstado(StringUtils.left(rede.getDsEstado(), 60));
			lojaUpdate.get().setDsCep(StringUtils.left(rede.getCdCep(), 9));
			lojaUpdate.get().setDsTelefone(tratarTelefone(rede.getDsTelefone()));
			lojaUpdate.get().setFlAceitacao(rede.getFlAceitacao());
			lojaUpdate.get().setCdTipoEstabelecimento(mapTipoEstabelecimentoCnae.get(rede.getCdCnae()));
			return lojaUpdate.get();
		} else {
			return LojaAceitacaoEntity.builder() //
					.cdCpfCnpj(rede.getCdCnpj()) //
					.dsNomeFantasia(StringUtils.left(rede.getDsNomeFantasia(), 60)) //
					.dsLogradouro(StringUtils.left(rede.getDsLogradouro(), 70)) //
					.dsBairro(StringUtils.left(rede.getDsBairro(), 60)) //
					.dsCidade(StringUtils.left(rede.getDsCidade(), 60)) //
					.dsEstado(StringUtils.left(rede.getDsEstado(), 60)) //
					.dsCep(StringUtils.left(rede.getCdCep(), 9)) //
					.dsTelefone(tratarTelefone(rede.getDsTelefone())) //
					.flAceitacao(rede.getFlAceitacao()) //
					.cdTipoBandeira(TIPO_BANDEIRA_MASTER) //
					.cdTipoEstabelecimento(mapTipoEstabelecimentoCnae.get(rede.getCdCnae())) //
					.build();
		}
	}

	private String tratarTelefone(String dsTelefone) {
		return StringUtils.left(dsTelefone.replaceAll("[^\\d]", StringUtils.EMPTY), 16);
	}

}
