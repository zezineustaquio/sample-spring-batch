package br.com.arquitetura.sample.spring.batch.processor;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.ThreadContext;
import org.springframework.batch.core.annotation.AfterProcess;
import org.springframework.batch.core.annotation.BeforeProcess;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import br.com.arquitetura.sample.spring.db.oracle.trb013.entity.LojaAceitacaoEntity;
import br.com.arquitetura.sample.spring.external.dto.geolocation.GeolocationResponseDTO;
import br.com.arquitetura.sample.spring.external.dto.geolocation.StatusEnum;
import br.com.arquitetura.sample.spring.external.repository.GoogleGeolocationRepository;
import lombok.extern.log4j.Log4j2;;

@Log4j2
@Component
public class GeolocalizacaoChunkProcessor implements ItemProcessor<LojaAceitacaoEntity, LojaAceitacaoEntity> {

	@Autowired
	private GoogleGeolocationRepository googleGeolocationRepository;

	@Value("#{'${google.api.geolocation.api-token}'.split(',')}")
	private List<String> lstApiToken;

	private int atualApiToken = 0;
	private boolean apiTokenExpired = Boolean.FALSE;

	@BeforeProcess
	public void beforeProcess(LojaAceitacaoEntity loja) {
		ThreadContext.put("CPF_CNPJ", loja.getCdCpfCnpj());
		log.info("GeolocalizacaoChunkProcessor iniciando...");
	}

	@Override
	public LojaAceitacaoEntity process(LojaAceitacaoEntity loja) throws Exception {
		if (loja != null
				&& (!loja.hashCodeAddress().equals(loja.getMapaClientes().getCdHash()) || !loja.isGeoLocated())) {

			loja.getMapaClientes().setDataUltimaAtualizacao(new Date());

			GeolocationResponseDTO location = geolocalizar(loja.getEnderecoCompleto());

			if (location != null && StatusEnum.OK.getStatus().equals(location.getStatus())
					&& !CollectionUtils.isEmpty(location.getResults())) {
				log.info("Atualizando dados da loja ...");
				loja.getMapaClientes().setCdHash(loja.hashCodeAddress());
				loja.setDsLatitude(location.getResults().get(0).getLatitude());
				loja.setDsLongitude(location.getResults().get(0).getLongitude());
				return loja;
			} else if (location != null && StatusEnum.ZERO_RESULTS.getStatus().equals(location.getStatus())) {
				loja.getMapaClientes().setCdHash(loja.hashCodeAddress());
				loja.setDsLatitude(BigDecimal.ZERO);
				return loja;
			} else {
				log.warn("Status de erro na requisição: {}", location != null ? location.getStatus() : "Payload NULL");
			}
		}
		return null;
	}

	@AfterProcess
	public void afterProcess(LojaAceitacaoEntity loja, LojaAceitacaoEntity result) {
		log.info("GeolocalizacaoChunkProcessor executado...");
		ThreadContext.remove("CPF_CNPJ");
	}

	private GeolocationResponseDTO geolocalizar(String enderecoCompleto) {
		if (apiTokenExpired) {
			return null;
		}
		log.info("Executando geolocalizacao na API Google ...");
		GeolocationResponseDTO location = googleGeolocationRepository.geolocate(enderecoCompleto,
				lstApiToken.get(atualApiToken));
		log.info("Requisicao concluida.");

		if (location == null || location.getStatus() == null) {
			log.warn("API não retornou corpo na requisição!");
			return null;
		}

		if (StatusEnum.OVER_QUERY_LIMIT.getStatus().equals(location.getStatus())) {
			atualApiToken++;
			log.warn("Limite de operações na API Google!");
			if (lstApiToken.size() <= atualApiToken) {
				log.warn("Não há mais APIs Token disponíveis!");
				log.warn("Processamento interrompido devido a limite da API Google!");
				apiTokenExpired = Boolean.TRUE;
				return null;
			}
			log.warn("Recuperando proxima API Token: {}", lstApiToken.get(atualApiToken));
			return geolocalizar(enderecoCompleto);
		}

		return location;
	}

}
