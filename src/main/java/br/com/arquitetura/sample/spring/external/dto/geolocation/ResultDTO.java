package br.com.arquitetura.sample.spring.external.dto.geolocation;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ResultDTO {
	@JsonProperty(value = "formatted_address")
	private String formatedAddress;

	@JsonProperty(value = "place_id")
	private String placeId;

	private GeometryDTO geometry;

	public BigDecimal getLatitude() {
		if (geometry != null && geometry.getLocation() != null)
			return geometry.getLocation().getLat();
		return null;
	}

	public BigDecimal getLongitude() {
		if (geometry != null && geometry.getLocation() != null)
			return geometry.getLocation().getLng();
		return null;
	}
}
