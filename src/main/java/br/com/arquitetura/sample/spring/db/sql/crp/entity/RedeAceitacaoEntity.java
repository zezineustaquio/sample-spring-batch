package br.com.arquitetura.sample.spring.db.sql.crp.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NamedNativeQueries({ //
		@NamedNativeQuery(name = "RedeAceitacaoEntity.listRedeAceitacao", //
				query = " SELECT DISTINCT" + //
						"	CD_CNAE," + //
						"	CLI.CD_CNPJ," + //
						"	CLI.DS_NOME_FANTASIA," + //
						"	CLI.DS_RAZAO_SOCIAL," + //
						"	ENDE.DS_LOGRADOURO," + //
						"	ENDE.DS_BAIRRO," + //
						"	ENDE.CD_CEP," + //
						"	TCD.DS_AREA AS 'DS_CIDADE'," + //
						"	TET.DS_AREA as 'DS_ESTADO'," + //
						"	CCOMU.VL_COMUNICACAO," + //
						"	CP.CN_PRODUTO," + //
						"	P.FL_ACEITACAO," + //
						"	P.DS_PRODUTO" + //
						" FROM" + //
						"	db_cadastro.dbo.TBL_CLIENTE CLI WITH(NOLOCK)" + //
						"	INNER JOIN db_cadastro.dbo.TBL_ENDERECO ENDE WITH(NOLOCK) ON CLI.CN_CODIGO = ENDE.CN_CODIGO"
						+ //
						"															AND ENDE.CN_TIPO_ENDERECO = 1" + //
						"	LEFT JOIN db_cadastro.dbo.TBL_AREA TCD WITH(NOLOCK) ON TCD.CN_AREA = ENDE.CN_AREA" + //
						"	LEFT JOIN db_cadastro.dbo.TBL_AREA TET WITH(NOLOCK) ON TET.CN_AREA = TCD.CN_AREA_PAI" + //
						"	LEFT JOIN db_cadastro.dbo.TBL_REDE RED WITH(NOLOCK) ON CLI.CN_REDE = RED.CN_REDE" + //
						"	LEFT JOIN db_gerencial.dbo.TBL_CLIENTE_ORIGEM_ESTABELECIMENTO_EMISSOR COE WITH(NOLOCK) ON CLI.CN_CODIGO = COE.CN_CODIGO"
						+ //
						"	LEFT JOIN db_gerencial.dbo.TBL_ESTABELECIMENTO EST WITH(NOLOCK) ON COE.CN_ESTABELECIMENTO = EST.CN_ESTABELECIMENTO"
						+ //
						"																	AND COE.CN_EMISSOR = EST.CN_EMISSOR"
						+ //
						"	LEFT JOIN db_cadastro.dbo.TBL_PRODUTO P WITH(NOLOCK) ON P.CN_PRODUTO = CLI.CN_PRODUTO" + //
						"	LEFT JOIN db_cadastro.dbo.TBL_CLIENTE_PRODUTO CP WITH(NOLOCK) ON CP.CN_CODIGO = CLI.CN_CODIGO"
						+ //
						"																	AND CP.CN_PRODUTO = P.CN_PRODUTO"
						+ //
						"	LEFT JOIN db_cadastro.dbo.TBL_CONTATO CONT ON CLI.CN_CODIGO = CONT.CN_CODIGO" + //
						"	LEFT JOIN db_cadastro.dbo.TBL_CONTATO_COMUNICACAO CCOMU ON CONT.CN_CONTATO = CCOMU.CN_CONTATO"
						+ //
						"															AND CONT.ID_PRINCIPAL = 'S'" + //
						"	LEFT JOIN db_cadastro.dbo.TBL_TIPO_COMUNICACAO TCOMU ON TCOMU.CN_TIPO_COMUNICACAO = CCOMU.CN_TIPO_COMUNICACAO"
						+ //
						" WHERE" + //
						"	CP.CN_STATUS_CLIENTE = 1" + //
						"	AND TCOMU.CN_TIPO_COMUNICACAO = 1 ", //
				resultClass = RedeAceitacaoEntity.class) })
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class RedeAceitacaoEntity implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "CD_CNPJ")
	private String cdCnpj;

	@Column(name = "CD_CNAE")
	private Long cdCnae;

	@Column(name = "DS_NOME_FANTASIA")
	private String dsNomeFantasia;

	@Column(name = "DS_RAZAO_SOCIAL")
	private String dsRazaoSocial;

	@Column(name = "DS_LOGRADOURO")
	private String dsLogradouro;

	@Column(name = "DS_BAIRRO")
	private String dsBairro;

	@Column(name = "CD_CEP")
	private String cdCep;

	@Column(name = "DS_CIDADE")
	private String dsCidade;

	@Column(name = "DS_ESTADO")
	private String dsEstado;

	@Column(name = "VL_COMUNICACAO")
	private String dsTelefone;

	@Column(name = "CN_PRODUTO")
	private String cnProduto;

	@Column(name = "FL_ACEITACAO")
	private Boolean flAceitacao;

	@Column(name = "DS_PRODUTO")
	private String dsProduto;

}
