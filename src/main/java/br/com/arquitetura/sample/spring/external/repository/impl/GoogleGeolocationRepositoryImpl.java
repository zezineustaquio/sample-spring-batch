package br.com.arquitetura.sample.spring.external.repository.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;

import br.com.arquitetura.sample.spring.external.dto.geolocation.GeolocationResponseDTO;
import br.com.arquitetura.sample.spring.external.repository.GoogleGeolocationRepository;

@Repository
public class GoogleGeolocationRepositoryImpl implements GoogleGeolocationRepository {

	@Value("${google.api.geolocation.endpoint}")
	private String endpoint;

	private static final String URL_PATTERN = "%s/json?key=%s&address=%s";

	@Autowired
	private RestTemplate rest;

	@Override
	public GeolocationResponseDTO geolocate(String address, String apiToken) {
		ResponseEntity<GeolocationResponseDTO> retorno = rest
				.getForEntity(String.format(URL_PATTERN, endpoint, apiToken, address), GeolocationResponseDTO.class);
		if (retorno.getBody() != null) {
			return retorno.getBody();
		}
		return null;
	}

}
