package br.com.arquitetura.sample.spring.external.dto.geolocation;

import java.math.BigDecimal;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class LocationDTO {
	private BigDecimal lat;
	private BigDecimal lng;
}
