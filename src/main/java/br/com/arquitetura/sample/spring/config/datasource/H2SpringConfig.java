package br.com.arquitetura.sample.spring.config.datasource;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

/**
 * Configuração do Datasource H2 em memória para configurações Spring Batch
 * 
 */
@Profile("!test")
@Configuration
@EnableTransactionManagement
@ConfigurationProperties(prefix = "datasource.h2.spring")
@EnableJpaRepositories(//
		basePackages = "br.com.arquitetura.sample.spring.db.h2.spring.repository", //
		entityManagerFactoryRef = "h2-spring-em", //
		transactionManagerRef = "h2-spring-tm")
public class H2SpringConfig extends HikariConfig {

	/**
	 * Factory para criação do Data Source
	 * 
	 * @return
	 */
	@Bean(name = "h2-spring-ds")
	@Primary
	public DataSource h2SpringDataSourceFactory() {
		return new HikariDataSource(this);
	}

	/**
	 * Factory para criação do Entity Manager
	 * 
	 * @param builder
	 * @return
	 */
	@PersistenceContext(unitName = "H2")
	@Bean(name = "h2-spring-em")
	@Primary
	public LocalContainerEntityManagerFactoryBean h2SpringEntityManagerFactory(EntityManagerFactoryBuilder builder) {
		return builder.dataSource(h2SpringDataSourceFactory()).persistenceUnit("H2").properties(jpaProperties())
				.packages("br.com.arquitetura.sample.spring.db.h2.spring.entity").build();
	}

	/**
	 * Factory para criação do Transaction Manager
	 * 
	 * @param em
	 * @return
	 */
	@Bean(name = "h2-spring-tm")
	@Primary
	public PlatformTransactionManager oracleTrb013TransactionManagerFactory(
			@Qualifier("h2-spring-em") EntityManagerFactory em) {
		return new JpaTransactionManager(em);
	}

	/**
	 * Propriedades do Persistence Unity
	 * 
	 * @return
	 */
	private Map<String, Object> jpaProperties() {
		Map<String, Object> props = new HashMap<>();
		props.put("hibernate.dialect", "org.hibernate.dialect.H2Dialect");
		return props;
	}
}
