package br.com.arquitetura.sample.spring.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.arquitetura.commons.exception.BusinessException;
import br.com.arquitetura.commons.exception.InfrastructureException;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "Geolocalizacao de Rede de Aceitacao")
@RestController
public class GeolocalizacaoController {

	private static final String JOB_SUCESSO = "Processamento da Rede de aceitação conluido!";

	@Autowired
	private JobLauncher jobLauncher;

	@Autowired
	@Qualifier("RedeAceitacaoJob")
	private Job redeAceitacaoJob;

	@Autowired
	@Qualifier("GeolocalizacaoJob")
	private Job geolocalizacaoJob;

	@Scheduled(cron = "${batch.rede-aceitacao.schedule}")
	@RequestMapping(method = RequestMethod.GET, value = "/rede-aceitacao")
	@ApiOperation(value = "Executa a carga de dados da rede na base MEFV")
	public ResponseEntity<String> processarRedeAceitacao() {
		return jobRunner(redeAceitacaoJob);
	}

	@Scheduled(cron = "${batch.geolocalizacao.schedule}")
	@RequestMapping(method = RequestMethod.GET, value = "/geolocalizacao")
	@ApiOperation(value = "Realiza a Geolocalização dos estabelecimentos na base MEFV")
	public ResponseEntity<String> geolocalizarRede() {
		return jobRunner(geolocalizacaoJob);
	}

	private ResponseEntity<String> jobRunner(Job job) throws BusinessException, InfrastructureException {
		try {
			JobParameters jobParameters = new JobParametersBuilder().toJobParameters();
			JobExecution execution = jobLauncher.run(job, jobParameters);
			if (execution.getExitStatus().equals(ExitStatus.COMPLETED)) {
				return new ResponseEntity<>(JOB_SUCESSO, HttpStatus.OK);
			} else {
				List<String> exceptionMessages = execution.getAllFailureExceptions().stream()
						.map(e -> ExceptionUtils.getRootCauseMessage(e)).collect(Collectors.toList());
				throw new BusinessException(StringUtils.join(exceptionMessages, "|"));
			}
		} catch (Exception e) {
			throw new InfrastructureException(e);
		}
	}
}
