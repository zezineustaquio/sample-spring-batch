package br.com.arquitetura.sample.spring.exception;

public class GoogleIntegrationException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public GoogleIntegrationException(String message) {
		super(message);
	}
}
