package br.com.arquitetura.sample.spring.db.oracle.trb013.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "TBL_MEFV_MAPA_CLIENTES", schema = "MEFV")
public class MapaClientesEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "CD_CPF_CNPJ")
	private String cdCpfCnpj;

	@Column(name = "CD_COMERCIAL")
	private Long cdComercial;
	
	@Column(name = "DS_LATITUDE")
	private BigDecimal dsLatitude;
	
	@Column(name = "DS_LONGITUDE")
	private BigDecimal dsLongitude;
	
	@Column(name = "DT_ULTIMA_ATUALIZACAO")
	private Date dataUltimaAtualizacao;
	
	@Column(name = "DT_ULTIMA_ATUALIZACAO_MOBILE")
	private Date dtUltimaAtualizacaoMobile;
	
	@Column(name = "CD_HASH")
	private String cdHash;
	
	@Column(name = "FL_DELETE")
	private Boolean flDelete;
}
