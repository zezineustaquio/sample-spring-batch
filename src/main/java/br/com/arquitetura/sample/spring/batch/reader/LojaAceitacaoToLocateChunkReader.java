package br.com.arquitetura.sample.spring.batch.reader;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.time.DateUtils;
import org.springframework.batch.core.annotation.AfterRead;
import org.springframework.batch.core.annotation.BeforeRead;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import br.com.arquitetura.sample.spring.db.oracle.trb013.entity.LojaAceitacaoEntity;
import br.com.arquitetura.sample.spring.db.oracle.trb013.repository.LojaAceitacaoRepository;
import lombok.extern.log4j.Log4j2;

@Log4j2
@Component
public class LojaAceitacaoToLocateChunkReader implements ItemReader<LojaAceitacaoEntity> {

	@Autowired
	private LojaAceitacaoRepository lojaAceitacaoRepository;

	@Value("${geolocalizacao.minimo.dias}")
	private int minDaysGeolocate;

	List<LojaAceitacaoEntity> lojas;
	private boolean searchReady = Boolean.FALSE;

	@BeforeRead
	public void beforeRead() {
		log.info("LojaAceitacaoToLocateChunkReader iniciado...");
	}

	@Override
	public LojaAceitacaoEntity read()
			throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		if (!searchReady) {
			log.info("Quantidade de dias minimo da ultima atualizacao: {}", minDaysGeolocate);
			lojas = lojaAceitacaoRepository
					.findLojaAceitacaoToLocate(DateUtils.addDays(new Date(), minDaysGeolocate * -1));
			searchReady = Boolean.TRUE;
			log.info("Consulta concluida com {} registros.", lojas.size());
		}

		if (!lojas.isEmpty()) {
			return lojas.remove(0);
		}
		return null;
	}

	@AfterRead
	public void afterRead(LojaAceitacaoEntity result) {
		log.info("LojaAceitacaoToLocateChunkReader executado!");
	}

}
