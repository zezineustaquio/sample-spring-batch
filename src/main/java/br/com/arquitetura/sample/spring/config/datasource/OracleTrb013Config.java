package br.com.arquitetura.sample.spring.config.datasource;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

/**
 * Configuração do Datasource Oracle para instancia TRB009
 * 
 */
@Profile("!test")
@Configuration
@EnableTransactionManagement
@ConfigurationProperties(prefix = "datasource.oracle.trb013")
@EnableJpaRepositories(//
		basePackages = "br.com.arquitetura.sample.spring.db.oracle.trb013.repository", //
		entityManagerFactoryRef = "oracle-trb013-em", //
		transactionManagerRef = "oracle-trb013-tm")
public class OracleTrb013Config extends HikariConfig {
	/**
	 * Factory para criação do Data Source
	 * 
	 * @return
	 */
	@Bean(name = "oracle-trb013-ds")
	public DataSource oracleTrb013DataSourceFactory() {
		return new HikariDataSource(this);
	}

	/**
	 * Factory para criação do Entity Manager
	 * 
	 * @param builder
	 * @return
	 */
	@PersistenceContext(unitName = "TRB013")
	@Bean(name = "oracle-trb013-em")
	public LocalContainerEntityManagerFactoryBean oracleTrb013EntityManagerFactory(
			EntityManagerFactoryBuilder builder) {
		return builder.dataSource(oracleTrb013DataSourceFactory()).persistenceUnit("TRB013").properties(jpaProperties())
				.packages("br.com.arquitetura.sample.spring.db.oracle.trb013.entity").build();
	}

	/**
	 * Factory para criação do Transaction Manager
	 * 
	 * @param em
	 * @return
	 */
	@Bean(name = "oracle-trb013-tm")
	public PlatformTransactionManager oracleTrb013TransactionManagerFactory(
			@Qualifier("oracle-trb013-em") EntityManagerFactory em) {
		return new JpaTransactionManager(em);
	}

	/**
	 * Propriedades do Persistence Unity
	 * 
	 * @return
	 */
	private Map<String, Object> jpaProperties() {
		Map<String, Object> props = new HashMap<>();
		props.put("hibernate.dialect", "org.hibernate.dialect.Oracle10gDialect");
		return props;
	}
}
