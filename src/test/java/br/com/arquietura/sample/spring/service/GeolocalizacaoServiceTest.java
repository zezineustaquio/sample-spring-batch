package br.com.arquietura.sample.spring.service;

import java.math.BigDecimal;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.util.ReflectionTestUtils;

import br.com.arquitetura.sample.spring.batch.processor.GeolocalizacaoChunkProcessor;
import br.com.arquitetura.sample.spring.db.oracle.trb013.entity.LojaAceitacaoEntity;
import br.com.arquitetura.sample.spring.db.oracle.trb013.entity.MapaClientesEntity;
import br.com.arquitetura.sample.spring.external.dto.geolocation.GeolocationResponseDTO;
import br.com.arquitetura.sample.spring.external.dto.geolocation.GeometryDTO;
import br.com.arquitetura.sample.spring.external.dto.geolocation.LocationDTO;
import br.com.arquitetura.sample.spring.external.dto.geolocation.ResultDTO;
import br.com.arquitetura.sample.spring.external.dto.geolocation.StatusEnum;
import br.com.arquitetura.sample.spring.external.repository.GoogleGeolocationRepository;

public class GeolocalizacaoServiceTest {

	@InjectMocks
	private GeolocalizacaoChunkProcessor service;

	@Mock
	private GoogleGeolocationRepository googleGeolocationRepository;

	private LojaAceitacaoEntity loja;

	@Before
	public void initMocks() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void geolocalizarRedeAceitacaoTest() throws Exception {
		initDefaultMock();
		service.process(loja);

	}

	@Test
	public void geolocalizarRedeAceitacaoTest_LojaNull() throws Exception {
		initDefaultMock();
		service.process(null);

	}

	@Test
	public void geolocalizarRedeAceitacaoTest_LimitGoogle() throws Exception {
		initDefaultMock();

		Mockito.when(googleGeolocationRepository.geolocate(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(GeolocationResponseDTO.builder() //
						.status(StatusEnum.OVER_QUERY_LIMIT.getStatus()) //
						.build());

		service.process(loja);

	}

	@Test
	public void geolocalizarRedeAceitacaoTest_ZeroResultsGoogle() throws Exception {
		initDefaultMock();

		Mockito.when(googleGeolocationRepository.geolocate(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(GeolocationResponseDTO.builder() //
						.status(StatusEnum.ZERO_RESULTS.getStatus()) //
						.build());

		service.process(loja);

	}

	@Test
	public void geolocalizarRedeAceitacaoTest_PayloadNull() throws Exception {
		initDefaultMock();

		Mockito.when(googleGeolocationRepository.geolocate(Mockito.anyString(), Mockito.anyString())).thenReturn(null);

		service.process(loja);

	}

	@Test
	public void geolocalizarRedeAceitacaoTest_StatusNull() throws Exception {
		initDefaultMock();

		Mockito.when(googleGeolocationRepository.geolocate(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(GeolocationResponseDTO.builder().build());

		service.process(loja);

	}

	@Test
	public void geolocalizarRedeAceitacaoTest_after_before() throws Exception {
		initDefaultMock();
		service.afterProcess(loja, null);
		service.beforeProcess(loja);

	}

	private void initDefaultMock() {
		ReflectionTestUtils.setField(service, "lstApiToken", Arrays.asList("123", "456"));

		loja = LojaAceitacaoEntity.builder() //
				.cdCpfCnpj("11111111111111") //
				.dsBairro("Bairro") //
				.dsCep("38400000") //
				.dsCidade("Uberlandia") //
				.dsEstado("MG") //
				.dsLogradouro("Rua Jatai, 111") //
				.mapaClientes(MapaClientesEntity.builder() //
						.cdHash("2023036866") //
						.build()) //
				.build();

		Mockito.when(googleGeolocationRepository.geolocate(Mockito.anyString(), Mockito.anyString()))
				.thenReturn(GeolocationResponseDTO.builder() //
						.status(StatusEnum.OK.getStatus()) //
						.results(Arrays.asList(ResultDTO.builder() //
								.geometry(GeometryDTO.builder() //
										.location(LocationDTO.builder() //
												.lat(BigDecimal.ONE) //
												.lng(BigDecimal.ONE) //
												.build()) //
										.build()) //
								.build())) //
						.build());
	}
}
