package br.com.arquietura.sample.spring.service;

import java.util.Arrays;
import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.test.util.ReflectionTestUtils;

import br.com.arquitetura.sample.spring.batch.processor.RedeAceitacaoTaskletProcessor;
import br.com.arquitetura.sample.spring.db.oracle.trb013.entity.LojaAceitacaoEntity;
import br.com.arquitetura.sample.spring.db.sql.crp.entity.RedeAceitacaoEntity;

public class RedeAceitacaoServiceTest {

	@InjectMocks
	private RedeAceitacaoTaskletProcessor service;

	@Before
	public void initMocks() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void processarRedeAceitacaoTest() throws Exception {
		initDefaultMock();
		service.execute(null, null);
	}

	@Test
	public void afterBefore() {
		StepExecution stepExec = new StepExecution("Teste", new JobExecution(0L));
		stepExec.setExecutionContext(new ExecutionContext());

		service.afterStep(stepExec);
		service.beforeStep(stepExec);
	}

	@SuppressWarnings("serial")
	private void initDefaultMock() {
		ReflectionTestUtils.setField(service, "lstRedeAceitacao", Arrays.asList( //
				RedeAceitacaoEntity.builder() //
						.cdCnpj("11111111111111") //
						.dsTelefone("3432323232") //
						.build(), //
				RedeAceitacaoEntity.builder() //
						.cdCnpj("22222222222222") //
						.dsTelefone("3432323232") //
						.build()));
		ReflectionTestUtils.setField(service, "mapTipoEstabelecimentoCnae", new HashMap<Long, Long>() {
			{
				put(1234L, 1234L);
			}
		});
		ReflectionTestUtils.setField(service, "lstLojasAtuais", Arrays.asList(LojaAceitacaoEntity.builder() //
				.cdCpfCnpj("11111111111111") //
				.build()));

	}
}
