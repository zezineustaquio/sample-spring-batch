# Projeto Spring Batch
> Este projeto deve ser utilizado de referência para o desenvolviemnto de aplicações Java utilizando o Spring Framework em aplicações com processamento Batch.

## Atenção
#### Qualquer melhoria, correção ou adição de exemplos pode ser desenvolvida e solicitado um Merge Request para aprovação.
#### Aplicação baseada no projeto [sample-spring-business-api](http://git.tribanco.com.br/bibliotecas/sample-spring-business-api).

## Sumário

* [Estrutura do projeto](README.md#estrutura-de-pacotes-do-projeto)
* [Tasklet vs Chunk](README.md#tasklet-vs-chunk)
* [Estrutura Spring Batch](README.md#estrutura-spring-batch)
* [Criacao da Job](README.md#criacao-da-job)
    * [Tasklet Reader](README.md#tasklet-reader)
    * [Chunk Reader](README.md#chunk-reader)
    * [Tasklet Writer](README.md#tasklet-writer)
    * [Chunk Writer](README.md#chunk-writer)
    * [Tasklet Processor](README.md#tasklet-processor)
    * [Chunk Processor](README.md#chunk-processor)
    * [Tasklet Job](README.md#tasklet-job)
    * [Chunk Job](README.md#chunk-job)
* [Exposicao da execucao da Job](README.md#exposicao-da-execucao-da-job)

## Estrutura de pacotes do projeto
![Estrutura de pacotes](doc/estrutura-pacotes.png)
* **br.com.arquitetura.sample.spring:** Pacote base da aplicação
    * **batch**
        * **job:** Classes com a configuração das JOBs da aplicação. Essas classes são executadas pelo contexto do Spring Batch.
        * **processor:** Classes contendo a regra de negócio do processamento da aplicação.
        * **reader:** Classe contendo as consultas de dados da aplicação.
        * **writer:** Classes contendo a persistencia de dados da aplicação.
    * **config**
        * **healthcheck:** Implementação da classe de serviço de [HealthCheck](http://git.tribanco.com.br/bibliotecas/sample-spring-business-api/blob/master/README.md#health-check).
        * **rest:** Classe de configuração do contexto REST dentro da aplicação Batch.
    * **controller:** Camada de exposição das chamadas das JOBs em recursos REST.

## Tasklet vs Chunk
**`Tasklet:`** Cada `step` da Job executa uma `Tasklet` de forma granular e é utilizado para execução de queries, processamento de listas e procedimentos avulsos.

**`Chunk:`** Cada `step` da Job possui uma entrada de dados `(reader)`, processamento do registro `(processor)` e persistencia do registro `(writer)`. Utilizado para processamento de registros de uma lista ou cópia de arquivos etc... onde é necessário processar um registro por vez.

## Estrutura Spring Batch
Nas JOBs criadas temos uma estrutura padrão de entrada, processamento e saída, que se dão pelas respectivas camadas da aplicação `reader`, `processor` e `writer`. 

A utilização do Spring Batch realiza a otimização de tarefas paralelas e assíncronas facilitando o gerenciamento de `threads` que é feito de forma transparente de acordo com a configuração da JOB.

As regras de negócio das JOBs devem estar apenas na camada `processor` que deve ser coberta pelos testes unitários da aplicação.

Com essa abordagem na criação da Aplicação, ela permanecerá em execução como serviço na máquina e as Jobs inativas aguardando um gatilho. A execução das Jobs serão expostas na interface REST da camada `controller`, possibilitando o schedule a partir do Automate ou qualquer outra ferramenta de execução que for utilizada no Tribanco. Dessa forma também se faz possível a implementação do [HealthCheck](http://git.tribanco.com.br/bibliotecas/sample-spring-business-api/blob/master/README.md#health-check) da aplicação para melhor monitoramento da saúde da aplicação.

## Criacao da JOB
### Tasklet Reader
`Tasklet Reader` é utilizado quando necessário trabalhar o processamento de `listas de registros`. Portanto a consulta irá retornar uma lista e armazenar no contexto da execução JOB.

**ATENÇÃO:** Deve ser criado um `Tasklet Reader` para cada fonte de dados a ser utilizado para a aplicação.

![Tasklet Reader](doc/tasklet-reader.png)
* Classe deve implementar `Tasklet` e `StepExecutionListener`, assim devendo implementar os métodos:
    * **beforeStep:** Será executado antes da execução do `Tasklet`.
    * **execute:** Método para execução da extração dos dados.
    * **afterStep:** Será executado após a execução da `Tasklet`, será utilizado para armazenar a lista de registros no contexto da execução da JOB e retornar o status da execução da `Step`.

### Chunk Reader
`Chunk Reader` é utilizado quando necessário trabalhar o processamento de apenas `um registro por vez`.

![Chunk Reader](doc/chunk-reader.png)
* Classe deve implementar `ItemReader` passando como parametro o objeto que será retornado pelo `Chunk Reader`, assim devendo implementar os métodos:
    * **beforeRead:** Será executado antes da execução da consulta.
    * **read:** Será executado a consulta e retornad o objeto passado como parametro no `ItemReader`.
        * **searchReady:** Variável utilizada para executar a consulta uma única vez. Assim será retornado o registro da lista existente e remove-lo para controle dos registros.
    * **afterRead:** Será executado depois da execução da consulta.

### Tasklet Writer
`Tasklet Writer` é a classe para persistencia dos registros processados no modelo de `Tasklet`.

![Tasklet Writer](doc/tasklet-writer.png)
* Classe deve implementar `Tasklet` e `StepExecutionListener`, assim devendo implementar os métodos:
    * **beforeStep:** Será executado antes da execução do `Tasklet`.
    * **execute:** Método para execução da persistencia dos dados.
    * **afterStep:** Será executado após a execução da `Tasklet` e retornar o status da execução da `Step`.

### Chunk Writer
`Chunk Writer` é utilizado para persistencia dos dados no modelo `Chunk`. A classe é invocada pela JOB de acordo com o intervalo de commit definido, assim pode ser persistido `um ou mais` registros de acordo com a configuração.

![Chunk Writer](doc/chunk-writer.png)
* Classe deve implementar `ItemWriter` passando como parametro o objeto que será persistido pelo `Chunk Writer`, assim devendo implementar os métodos:
    * **beforeStep:** Será executado antes da execução da persistencia.
    * **write:** Será executado a persistencia de `um ou mais` registros de acordo com o intervalo de commit configurado na JOB.
    * **afterStep:** Será executado depois da execução da persistencia e retornar o status da execução da `Step`.

### Tasklet Processor
`Tasklet Processor` é utilizado para processamento das informações no modelo de `Tasklet`, onde deve conter as regras de negócio da JOB.

![Tasklet Processor](doc/tasklet-processor.png)
* Classe deve implementar `Tasklet` e `StepExecutionListener`, assim devendo implementar os métodos:
    * **beforeStep:** Será executado antes da execução do `Tasklet`. Responsável por carregar as informações de listas do contexto da JOB para utilização no processamento das regras de negócio.
    * **execute:** Método para processamento dos registros de acordo com as regras de negócio da JOB.
    * **afterStep:** Será executado após a execução da `Tasklet`. Os registros devem ser removidos do contexto da JOB e retornar o status da execução da `Step`.

### Chunk Processor
`Chunk Processor` é utilizado para processamento dos itens no modelo `Chunk`, onde cada item é processado de forma idependente sendo aplicadas as regras de negócio da JOB.

![Chunk Processor](doc/chunk-processor.png)
* Classe deve implementar `ItemProcessor` passando como parametros os objetos de `Input` retornado pelo `Chunk Reader` e `Output` do processamento, assim devendo implementar os métodos:
   * **beforeProcess:** Será executado antes do processamento do registro e recebe como parametro o objeto de entrada retornado pelo `Chunk Reader`.
    * **process:** Método para processamento do registro de acordo com as regras de negócio da JOB, retornando o objeto especificado na Interface `ItemProcessor`.
    * **afterProcess:** Será executado após a execução do processamento do registro, recebe como parametros o objeto de entrada e de saída do processamento.

### Tasklet Job
`Tasklet Job` deve extender `JobExecutionListenerSupport` e anotado como `@Component` para que o contexto do Spring Batch realize a execução da JOB.
Devem ser declarados os objetos `JobBuilderFactory` e `StepBuilderFactory` para construção das JOBs e Steps.
Também devem ser declarados os `Tasklets` de Reader, Processor e Writer que serão utilizados.

![Tasklet Job Params](doc/tasklet-job-params.png)
A criação da JOB deve ser realizada pela implementação de um método anotado com `@Bean` contendo o nome da JOB, onde deve retornar um Objeto `Job` criado pelo `JobBuilderFactory`.

![Tasklet Job - Job Factory](doc/tasklet-job-jobfactory.png)
`JobBuilderFactory` deve criar as jobs contendo `Steps`, informando no metodo `start` a primeira a ser executada e vários métodos `next` contendo os passos da JOB a ser executada. Cada `step` deve ser criado utilizando `StepBuilderFactory`.

`StepBuilderFactory` deve criar os steps da JOB que podem receber um `Tasklet` ou `Flow` dependendo da necessidade:
   * **Tasklet:** Realiza a execução das Tasklets configuradas anteriormente para executar as acões de forma síncrona.
   * **Flow:** É utilizado para configuração de `Steps` para serem executadas de forma paralela ou assíncrona de acordo com a necessidade da JOB.

A classe `JobExecutionListenerSupport` permite que seja sobrescrito o método `afterJob` que será executado no término do processamento da JOB.

### Chunk Job
`Chunk Job` deve extender `JobExecutionListenerSupport` e anotado como `@Component` para que o contexto do Spring Batch realize a execução da JOB.
Devem ser declarados os objetos `JobBuilderFactory` e `StepBuilderFactory` para construção das JOBs e Steps.
Também devem ser declarados os ChunkReader, ChunkProcessor e ChunkWriter que serão utilizados.

![Chunk Job](doc/chunk-job.png)
A diferença na criação de uma JOB utilizando o modelo `Chunk` é a criação da `Step` que deve conter `reader`, `writer`, `processor` e o `chunk` com o intervalo de commit dos registros. O restante da estrutura é exatamente a mesma.

## Exposicao da execucao da Job
Na camada de `controller` temos as classes para exposição das chamadas para inicio do processamento das Jobs através de interface REST.

Será utilizado o `JobLauncher` para execução do contexto da Job conforme exemplo abaixo:
![Job Controller](doc/controller.png)
